(function($, window){
    $(document).ready(function() {
        $(".menu-button").click(function(e) {
            e.preventDefault();
            $("#top-menu-normal").toggle('slow');
        });
    });
})(jQuery, window);