<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add Stylesheets

$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');
$doc->addScript(JURI::root(true).'/media/system/js/jquery.min.js');
$doc->addScript('templates/'.$this->template.'/js/flexnav/jquery.flexnav.js');
$doc->addStyleSheet('templates/'.$this->template.'/js/flexnav/flexnav.css');

// Add current user information
$user = JFactory::getUser();
?>
<?php if(!$params->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
	<?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	   
	<jdoc:include type="head" />

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
<script>
jQuery.noConflict();
(function (window, $) {
    function inicioDoc() {
        $(".flexnav").flexNav({ 'buttonSelector': '.menu-button' });
    }
    $(document).ready(inicioDoc);
})(window, jQuery);	
</script>
</head>

<body>    
    <div class="header">			
        <jdoc:include type="modules" name="banner-header" style="xhtml" />
        <div id="top-menu">
            <div id="top-menu-normal">
                <jdoc:include type="modules" name="top-menu" style="none" />
                <a href="#" class="co"></a>
                <a href="#" class="de"></a>
                <div style="clear: both"></div>
            </div>
            <div id="top-menu-mini">
                <div class="menu-button">Menu</div>
                <div class="flexnav" data-breakpoint="900" style="z-index: 1000">
                    <jdoc:include type="modules" name="top-menu" style="none" />
                    <a style="margin-left: 10px; margin-right: 10px;" href="#" class="co"></a>
                    <a style="margin-left: 10px; margin-right: 10px;" href="#" class="de"></a>
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
        <div id="logo_header">
            <div id="banda_logo"></div>
            <img src="templates/<?php echo $this->template; ?>/img/dscali.png" />
        </div>
        <div style="clear: both"></div>
    </div>
    <!-- Body -->
    <div id="cuerpo" class="ancho_doc">
        <div>   
            <div class="contenido">
                <div class="sidebar">
                    <div id="menu-ppal">					
                        <jdoc:include type="modules" name="side-bar" style="xhtml" />
                    </div>
                    <div style="clear: both"></div>
                </div>
                <div class="componente">
                    <jdoc:include type="modules" name="position-0" style="xhtml" />
                    <jdoc:include type="message" />
                    <jdoc:include type="component" />
                    <jdoc:include type="modules" name="position-1" style="none" />
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
    </div>
    <?php if($this->countModules('modulos-inferiores1') or $this->countModules('modulos-inferiores2') or $this->countModules('modulos-inferiores3') or $this->countModules('modulos-inferiores4')) : ?>
    <div class="modulos-inferiores ancho_doc">
        <div>
            <div class="mod-inferior">
                <jdoc:include type="modules" name="modulos-inferiores1" style="none" />
            </div>
            <div class="mod-inferior">
                <jdoc:include type="modules" name="modulos-inferiores2" style="none" />
            </div>
            <div class="mod-inferior">
                <jdoc:include type="modules" name="modulos-inferiores3" style="none" />
            </div>
            <div class="mod-inferior">
                <jdoc:include type="modules" name="modulos-inferiores4" style="none" />
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
    <?php endif; ?>
    <div class="mod-slider ancho_doc">
        <div>
        <jdoc:include type="modules" name="mod-slider" style="none" />
        </div>
    </div>
    <div class="menu-inf ancho_doc">
        <div>
        <jdoc:include type="modules" name="menu-footer" style="none" />
        </div>
    </div>
    <!-- Footer -->
    <div class="footer ancho_doc">
        <div>
            <div class="modulos-footer">
                <jdoc:include type="modules" name="footer" style="none" />
            </div>
            <div class="redes-sociales">
                <a href="http://www.picasa.com" class="picasa"></a>
                <a href="http://meneame.net" class="red"></a>
                <a href="http://youtube.es" class="youtube"></a>
                <a href="http://twitter.com" class="twitter"></a>
                <a href="http://facebook.com" class="facebook"></a>
                <div style="float: none; clear: both"></div>
            </div>
            <div style="float: none; clear: both"></div>            
        </div>
    </div>
    <jdoc:include type="modules" name="debug" style="none" />    
</body>
</html>
