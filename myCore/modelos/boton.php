<?php

class Boton extends myEloquent {    
    protected $table = 'my_boton';
    
    protected $fillable = array('nombre', 'clase', 'url');
}
