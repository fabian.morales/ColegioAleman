<?php
/*
	Héctor Fabián Morales Ramírez
	Tecnólogo en Ingeniería de Sistemas
	Enero 2011
*/

use \Illuminate\Database\Capsule\Manager as Capsule;  

class myModelo{
    private $db;
    
    public static function boot(){
        myApp::getEloquent();
        
        $modelos = glob(dirname(__DIR__).DS."modelos".DS."*.php");

        foreach ($modelos as $m){
            require_once $m;            
        }
    }
    
    function myModelo(){
        $this->db = JFactory::getDBO();        
    }

    function insertId(){
        return $this->db->insertid();
    }

    function setQuery($sql){
        $this->db->setQuery($sql);
    }

    function loadAssoc(){
        return $this->db->loadAssoc();
    }

    function loadAssocList(){
        return $this->db->loadAssocList();
    }

    function query(){
        return $this->db->query();
    }
    
    function getGrupoUser(){
        $sql = "select * from #__usergroups where title = 'Registered'";
        $this->setQuery($sql);
        return $this->loadAssoc();
    }

    function getUsuarioGrupo($idUser){
        $sql = "select * from #__user_usergroup_map where user_id = ".$idUser;
        $this->setQuery($sql);
        return $this->loadAssoc();
    }

    function guardarUsuarioGrupo($idUser, $grupo){
    	$sql = "";

        if (!sizeof($this->getUsuarioGrupo($idUser))){
            $sql = "insert into #__user_usergroup_map(user_id, group_id) values (".$idUser.", ".$grupo.")";
        }
        else{
            $sql = "update #__user_usergroup_map set group_id = ".$grupo." where user_id = ".$idUser;
        }
        
        $this->setQuery($sql);
        return $this->query();
    }
}
?>