<?php

abstract class myController{
    public function ejecutar($tarea = "index", $args = []){
        if (!$tarea){
            $tarea = "index";
        }

        if (method_exists($this, $tarea)){
            //echo $this->$tarea();
            $func = [$this, $tarea];
            echo call_user_func_array($func, $args);
        }
        else{
            myApp::mostrarMensaje("Opción no válida", "error");
        }        
    }
    
    public abstract function index();    
}

