<?php

if (!defined("DS")){
    define("DS", DIRECTORY_SEPARATOR);
}

    $coreDir = __DIR__."/";
    $clasesDir = $coreDir."clases/";
    $libDir = $coreDir."lib/";
    $archivos = [
        $clasesDir."myApp.php",
        $clasesDir."myConfig.php", 
        $clasesDir."myDocumento.php", 
        $clasesDir."myFunciones.php", 
        $clasesDir."myModelo.php",        
        $clasesDir."myController.php",
        $clasesDir."myAdminController.php",
        $clasesDir."myView.php",        
        $clasesDir."myRequest.php", 
        $libDir.'laravel/autoload.php',
        $clasesDir.'myEloquent.php',
        $libDir."Twig/Autoloader.php",
        $libDir."mpdf/mpdf.php",
        $coreDir."pagos/myPlataformaPago.php"];

    foreach ($archivos as $a){
        require_once $a;
    }

    myModelo::boot();
    myView::boot();