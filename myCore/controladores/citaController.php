<?php

class citaController extends myController{
    public function index(){
        return myView::render("cita.form");
    }
    
    public function enviarCorreoCita(){
        $contacto = array();
        $contacto["nombre"] = myApp::getRequest()->getVar("nombre");
        $contacto["tel1"] = myApp::getRequest()->getVar("tel1");
        $contacto["tel2"] = myApp::getRequest()->getVar("tel2");
        $contacto["email"] = myApp::getRequest()->getVar("email");
        $contacto["fecha"] = myApp::getRequest()->getVar("fecha");
        $contacto["asunto"] = 'Solicitud cita';
        $contacto["url"] = JUri::root();
        
        $jcfg = new JConfig();        
        $cfg = new myConfig();
        $mail = JFactory::getMailer();
        $mail->addRecipient($jcfg->mailfrom);
        //$mail->addBCC($cfg->correoAdmin);
        $mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
        $mail->setSubject("Solicitud cita");
        $mail->addBCC("desarrollo@encubo.ws");        
        $mail->IsHTML(1);
        $mail->setBody(myView::render("cita.correo", array("contacto" => $contacto)));
        $envio = $mail->Send();
        
        if ( $envio !== true ) {
            return 'Ha ocurrido un error enviando el correo: '.$envio->message;
        } 
        else {
            return 'Se ha envido un correo con la solicitud de cita';
        }
    }
    
    public function enviarCorreoContacto(){
        $contacto = array();
        $contacto["nombre"] = myApp::getRequest()->getVar("nombre");
        $contacto["tel1"] = myApp::getRequest()->getVar("tel1");
        $contacto["tel2"] = myApp::getRequest()->getVar("tel2");
        $contacto["email"] = myApp::getRequest()->getVar("email");
        $contacto["comentarios"] = myApp::getRequest()->getVar("comentarios");
        $contacto["asunto"] = 'Solicitud de contacto';
        $contacto["url"] = JUri::root();
        
        $jcfg = new JConfig();        
        $cfg = new myConfig();
        $mail = JFactory::getMailer();
        $mail->addRecipient($jcfg->mailfrom);
        //$mail->addBCC($cfg->correoAdmin);
        $mail->setSender(array($jcfg->mailfrom, $jcfg->fromname));
        $mail->setSubject("Solicitud de contacto");
        $mail->addBCC("desarrollo@encubo.ws");        
        $mail->IsHTML(1);
        $mail->setBody(myView::render("cita.correo", array("contacto" => $contacto)));
        $envio = $mail->Send();
        
        if ( $envio !== true ) {
            return 'Ha ocurrido un error enviando el correo: '.$envio->message;
        } 
        else {
            return 'Se ha envido un correo con la solicitud de cita';
        }
    }
}