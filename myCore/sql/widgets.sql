create table arc_my_wid_widget(
    id integer not null auto_increment primary key,
    tipo varchar(20) not null,
    nombre varchar(100) not null,
    clase_css varchar(200),
    ancho varchar(10),
    alto varchar(10),
    config text,
    created_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE InnoDB;

create table arc_my_wid_slideritem(
    id integer not null auto_increment primary key,
    id_widget integer not null,
    url varchar(400),    
    titulo varchar(100),
    imagen varchar(400),
    created_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_widget) references arc_my_wid_widget (id)
) ENGINE InnoDB;

create table arc_my_wid_mapapunto(
    id integer not null auto_increment primary key,
    id_widget integer not null,
    descripcion varchar(100),
    latitud numeric(10, 8) default 0,
    longitud numeric(10, 8) default 0,
    imagen varchar(400),    
    info text,
    centro char(1) default 'N',
    created_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_widget) references arc_my_wid_widget (id)
) ENGINE InnoDB;

create table arc_my_wid_galeriaitem(
    id integer not null auto_increment primary key,
    id_widget integer not null,
    url varchar(400),    
    titulo varchar(100),
    created_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_widget) references arc_my_wid_widget (id)
) ENGINE InnoDB;

create table arc_my_wid_redsocial(
    id integer not null auto_increment primary key,
    id_widget integer not null,
    url varchar(400),    
    tipo varchar(100),
    created_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_widget) references arc_my_wid_widget (id)
) ENGINE InnoDB;

