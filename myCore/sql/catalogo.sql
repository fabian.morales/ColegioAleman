create table arc_my_cat_categoria(
	id integer not null auto_increment,
	nombre varchar(60),
	id_cat integer,
	primary key(id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table arc_my_cat_referencia(
	id integer not null auto_increment,
	nombre varchar(60),
	referencia varchar(20),
	descripcion varchar(500),	
	valor_base numeric(10,3),
	fecha timestamp,
	existencias integer default 0,
    tipo char(1) default 'N',
	primary key (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table arc_my_cat_conjuntoref(
	id_conjunto integer,
	id_referencia integer,
	foreign key (id_conjunto) references arc_my_cat_referencia (id) on delete cascade,
	foreign key (id_referencia) references arc_my_cat_referencia (id) on delete cascade,
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table arc_my_cat_catref(
	id_categoria integer,
	id_referencia integer,
	foreign key (id_referencia) references arc_my_cat_referencia (id),
	foreign key (id_categoria) references arc_my_cat_categoria (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 	
);

create table arc_my_cat_color(
	id integer not null auto_increment,
	nombre varchar(60),
	codigo varchar(10),
	thumb varchar(255),
	primary key (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table arc_my_cat_talla(
	id integer not null auto_increment,
	nombre varchar(60),
	abrev varchar(10),
	primary key (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table arc_my_cat_extension(
	id integer not null auto_increment,
	id_talla integer default null,
	id_color integer default null,
	id_referencia integer not null,
	mod_valor numeric(10, 3),
	thumb varchar(255),
	primary key (id),
	foreign key (id_referencia) references arc_my_cat_referencia (id) on delete cascade,
	foreign key (id_talla) references arc_my_cat_talla (id),
	foreign key (id_color) references arc_my_cat_color (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE InnoDB;

create table arc_my_cat_atributo(
	id integer not null auto_increment primary key,
	portada enum('S', 'N') default 'N',
	descripcion varchar(60) not null,
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
);

create table arc_my_cat_atributoref(
	id integer not null auto_increment primary key,
	id_atributo integer not null,
	id_referencia integer not null,
	valor varchar(300) default ' ',
	foreign key (id_atributo) references arc_my_cat_atributo (id) on delete cascade,
	foreign key (id_referencia) references arc_my_cat_referencia (id) on delete cascade,
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
);

CREATE TABLE arc_my_cat_imgref (
	id INT(11) NOT NULL AUTO_INCREMENT,
	id_referencia INT(11) NULL DEFAULT NULL,	
	descripcion VARCHAR(60) NULL DEFAULT NULL,
	archivo VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_referencia) REFERENCES arc_my_cat_referencia (id),
	created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
) ENGINE=InnoDB;

