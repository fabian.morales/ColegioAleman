(function($, window){
    $(document).ready(function () {
        $("a[rel='fancy']").fancybox({ type: 'ajax', 'arrows':false });
        
        $("a[rel='lnkBorrarPunto']").click(function (e) {
            e.preventDefault();
            if (confirm('¿Está seguro de borrar este punto?')){
                window.location.href = $(this).attr("href");
            }
        });
    });
})(jQuery, window); 