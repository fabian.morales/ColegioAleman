/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function (window, $) {
    function hookFormExtensiones() {
        $("#btnGuardarExt").unbind("click");
        $("#btnGuardarExt").bind("click", function (e) {
            e.preventDefault();
            $.ajax({
                url: 'index.php?option=com_my_component&controller=adminCatalogo&task=guardarExtension&format=raw',
                method: 'post',
                data: { id: $("#id_ext").val(), id_color: $("#id_color_ext").val(), id_talla: $("#id_talla_ext").val(), mod_valor: $("#mod_valor_ext").val(), id_referencia: $("#id_referencia_ext").val() },
                success: function (res) {
                    $("#divListaExtensiones").html(res);
                    hookListaExtensiones();
                }
            });
        });
    }

    function hookListaExtensiones() {
        $("a[rel='eliminarExtension']").unbind("click");
        $("a[rel='eliminarExtension']").bind("click", function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                method: 'get',
                success: function (res) {
                    $("#divListaExtensiones").html(res);
                    hookListaExtensiones();
                }
            });
        });

        $("a[rel='modExtension']").unbind("click");
        $("a[rel='modExtension']").bind("click", function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                method: 'get',
                success: function (res) {
                    $("#divFormExtensiones").html(res);
                    hookListaExtensiones();
                }
            });
        });
    }

    function listarImagenes($id) {
        $.ajax({
            url: "index.php?option=com_my_component&controller=adminCatalogo&task=listarImagenes&format=raw&id=" + $("#id").val(),
            method: 'get',
            success: function (res) {
                $("#divListaImagenes").html(res);
                //hookListaExtensiones();
            }
        });
    }

    $(document).ready(function () {
        hookFormExtensiones();
        hookListaExtensiones();

        $("#upload_img").plupload({
            // General settings
            runtimes: 'html5,flash,silverlight,html4',
            url: "index.php?option=com_my_component&controller=adminCatalogo&task=agregarImagenes&format=raw&id=" + $("#id").val(),

            // Maximum file size
            max_file_size: '2mb',

            chunk_size: '1mb',

            // Resize images on clientside if we can
            resize: {
                width: 200,
                height: 200,
                quality: 90,
                crop: true // crop to exact dimensions
            },

            // Specify what files to browse for
            filters: [
                { title: "Image files", extensions: "jpg,gif,png" },
                { title: "Zip files", extensions: "zip,avi" }
            ],

            // Rename files by clicking on their titles
            rename: true,

            // Sort files
            sortable: true,

            // Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
            dragdrop: true,

            // Views to activate
            views: {
                list: false,
                thumbs: true, // Show thumbs
                active: 'thumbs'
            },

            // Flash settings
            flash_swf_url: '/plupload/js/Moxie.swf',

            // Silverlight settings
            silverlight_xap_url: '/plupload/js/Moxie.xap',

            init: {
                UploadComplete: function (up, files) {
                    // Called when all files are either uploaded or failed
                    //console.log(up);
                    listarImagenes($("#id").val());
                    alert('Completo');
                }
            }
        });
    });
})(window, jQuery);