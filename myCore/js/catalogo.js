(function (window, $) {
    function hookModuloCarrito() {
        $("#modCarrito a.boton-cantidad").click(function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                method: 'get',
                success: function (res) {
                    $("#modCarrito").fadeOut(200);
                    $("#modCarrito").html(res).fadeIn(200);
                    hookModuloCarrito();
                }
            });
        });
    }

    $(document).ready(function () {
        hookModuloCarrito();
        $("img[rel='thumbImagen']").click(function (e) {
            e.preventDefault();
            $(".imgThumbActiva").removeClass("imgThumbActiva");
            $(this).addClass("imgThumbActiva");
            $("#idProducto").attr("href", $(this).attr("data-img"));
            $("#idProducto > img").attr("src", $(this).attr("src"));
        });

        $("a[rel='lnkAgregarProd']").click(function (e) {
            e.preventDefault();
            $cantidad = $("#cantidad").val();
            $idExt = $("#idExt").val();
            $idProd = $("#id").val();

            $.ajax({
                url: 'index.php?option=com_my_component&controller=carrito&task=agregarProducto',
                method: 'post',
                data: { id_producto: $idProd, id_ext: $idExt, cantidad: $cantidad, format: 'raw' },
                success: function (res) {
                    $("#modCarrito").fadeOut(200);
                    $("#modCarrito").html(res).fadeIn(200);
                    hookModuloCarrito();
                }
            });
        });
    });
})(window, jQuery);