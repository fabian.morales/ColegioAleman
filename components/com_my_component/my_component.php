<?php
/*
	Héctor Fabián Morales Ramírez
	Tecnólogo en Ingeniería de Sistemas
	Enero 2011
*/

require_once(JPATH_ROOT."/myCore/autoload.php");
$c = myApp::getController(JRequest::getVar('controller', 'catalogo'));
$c->ejecutar(JRequest::getVar('task'));
